# Benodigdheden

## Apps

- Visual Studio Code
- Git

### Windows

- Git bash (zit bij Git install in)
- Python 2.7.10 als je issues hebt met builden

## VS Code extensions / mods

- Prettier
- Babel JavaScript
- vscode-styled-components

### Aanbevolen

- Bracket Pair Colorizer 2
- Import Cost
- Fira Code (Retina) Font + Font ligatures
- npm
- npm Intellisense

## NPM Packages

- styled-components
- react-router-dom

### Development

- prettier
