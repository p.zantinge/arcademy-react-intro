import { NavLink as RouterNavLink } from 'react-router-dom';
import styled from 'styled-components';

const NavLink = styled(RouterNavLink)`
  font-size: 14px;
  color: inherit;
  text-decoration: none;
  padding: 8px;
  display: inline-block;

  &:visited {
    color: inherit;
    text-decoration: none;
  }
  &:focus,
  &:hover {
    border-bottom: 2px solid salmon;
  }
  &.active {
    border-bottom: 2px solid red;
  }

  &:not(:last-child) {
    margin-right: 1rem;
  }
`;

export default NavLink;
