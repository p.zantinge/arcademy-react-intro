import React from 'react';
import styled from 'styled-components';
import TwoColumnSide from 'components/Layouts/TwoColumnSide';
import { Link } from 'react-router-dom';

const Content = styled.section`
  padding: 32px;
  > p {
    font-size: 14px;
    line-height: 22px;
    letter-spacing: 0.5px;
  }
`;

const StyledList = styled.ol`
  > li {
    font-size: 20px;
    line-height: 40px;
  }
`;

export default function Blog() {
  return (
    <TwoColumnSide>
      <Content>
        <h3>Hier komt een blog, als je de volgende opdrachten voltooid</h3>
        <StyledList>
          <li>
            Opdracht: Ga naar <Link to="/setup">de setup pagina</Link>. Hiermee
            laad je de blogs in je localstorage
          </li>
          <li>Opdracht: Haal alle blogs op uit de "API"</li>
          <li>Opdracht: Toon alle blogs onder elkaar</li>
          <li>Opdracht: Maak een pagina voor een enkele blog</li>
          <li>
            Opdracht: Zorg dat je op juiste pagina komt als je op een blogtitel
            in het overzicht klikt
          </li>
          <li>Bonus opdracht: doe alsjeblieft wat een deze styling....</li>
        </StyledList>
      </Content>
    </TwoColumnSide>
  );
}
