import React from 'react';
import FullWidth from 'components/Layouts/FullWidth';
import img from 'assets/img/hero.jpg';
import styled from 'styled-components';

const Hero = styled.img`
  width: 100vw;
  height: 450px;
  object-fit: cover;
  object-position: center center;
`;

const Content = styled.section`
  padding: 32px;
  > p {
    font-size: 14px;
    line-height: 22px;
    letter-spacing: 0.5px;
  }
`;

export default function Home() {
  return (
    <FullWidth>
      <Hero src={img} />
      <Content>
        <h1>
          Opdracht 1: Zorg dat de het Blog component/pagina gerenderd wordt als
          je op "Blog" klikt!
        </h1>
        <p>
          Leuke dingen om te vertellen. Maar niet zo leuk als Bacon ipsum dolor
          amet pork beef ham hock, short loin turkey jowl sausage biltong corned
          beef alcatra frankfurter t-bone salami shankle meatloaf. Frankfurter
          rump doner, chicken kevin short loin swine landjaeger andouille
          tri-tip flank pork chop ground round. Prosciutto andouille swine,
          meatloaf short ribs tongue cow turducken biltong sirloin kevin.
          Sausage flank ribeye meatloaf picanha porchetta pork spare ribs swine
          shoulder ham hock shankle turducken beef ribs pork loin. Ham beef ribs
          turkey, turducken andouille kevin cow ground round sausage. Flank tail
          shank, pastrami burgdoggen pork chop andouille alcatra cupim fatback.
          Short ribs pastrami corned beef, sausage beef ribs meatloaf landjaeger
          biltong bresaola flank.
        </p>
        <p>
          Swine picanha burgdoggen shoulder shankle boudin landjaeger meatloaf
          pork. Kielbasa short ribs strip steak corned beef. Jerky picanha pork
          loin, ribeye shank short loin fatback pork turducken ground round.
          Buffalo fatback swine pork loin pig rump. Swine turducken biltong pig
          brisket.
        </p>
      </Content>
    </FullWidth>
  );
}
