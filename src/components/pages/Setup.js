import React, { useEffect } from 'react';
import Layout from 'components/Layout';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import useLocalStorage from 'util/hooks/useLocalStorage';
import mockBlogs from 'util/mocks/blogs';

const Container = styled.div`
  display: flex;
  height: 100vh;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

export default function Setup() {
  const blogs = useLocalStorage('blogs');
  async function setupBlogs() {
    await blogs.save(mockBlogs);
  }

  useEffect(() => {
    setupBlogs();
  });
  return (
    <Layout>
      <Container>
        <h5>Alle blogs aan localstorage toegevoegd...</h5>
        <div>
          <Link to="/blog">Terug naar blog!</Link>
        </div>
      </Container>
    </Layout>
  );
}
