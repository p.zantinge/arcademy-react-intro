import React from 'react';
import Layout from 'components/Layout';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const Container = styled.div`
  display: flex;
  height: 100vh;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

export default function NotFound() {
  return (
    <Layout>
      <Container>
        <h1>Pagina niet gevonden</h1>
        <div>
          <Link to="/">Terug naar home!</Link>
        </div>
      </Container>
    </Layout>
  );
}
