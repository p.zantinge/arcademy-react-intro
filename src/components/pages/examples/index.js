import React from 'react';
import { Switch, Route, Redirect, useRouteMatch } from 'react-router-dom';
import LocalStorage from './LocalStorage';

export default function Examples() {
  const { path } = useRouteMatch();

  return (
    <Switch>
      <Route path={`${path}/localstorage`} exact component={LocalStorage} />
      <Redirect to={`/404`} />
    </Switch>
  );
}
