import React, { useState } from 'react';
import useLocalStorage from 'util/hooks/useLocalStorage';
import Layout from 'components/Layout';
import NavLink from 'components/NavLink';

export default function LocalStorage() {
  const test = useLocalStorage('test');
  const [val, setVal] = useState(test.load() || '');
  const [storedVal, setStoredVal] = useState(test.load());

  async function onSaveClick(e) {
    e.preventDefault();
    const result = await test.save(val);
    setStoredVal(result);
  }

  async function onLoadClick(e) {
    e.preventDefault();
    const result = await test.load(val);
    setVal(result);
  }

  function onInputChange(e) {
    setVal(e.target.value);
  }

  return (
    <Layout
      header={<span>LocalStorage</span>}
      aside={<button onClick={onLoadClick}>Load from storage</button>}
      leftNav={<button onClick={onSaveClick}>Save to storage</button>}
      footer={'FOOTERRRRRRR'}
      topNav={
        <div>
          <NavLink to="/" exact>
            Home
          </NavLink>
          <NavLink to="/examples/localstorage" exact>
            LocalStorage
          </NavLink>
        </div>
      }
    >
      <input value={val} onChange={onInputChange} />
      <ul>
        <li>Value in state: {val}</li>
        <li>Value in storage: {storedVal}</li>
      </ul>
    </Layout>
  );
}
