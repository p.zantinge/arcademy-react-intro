import React from 'react';
import Layout from 'components/Layout';
import styled from 'styled-components';
import NavLink from 'components/NavLink';

const Header = styled.h1`
  margin: 0;
  padding: 8px;
  font-size: 24px;
`;

const DefaultTopNav = () => (
  <>
    <NavLink to="/home" exact>
      Home
    </NavLink>
    <NavLink to="/about" exact>
      Over ons
    </NavLink>
    <NavLink to="/blog" exact>
      Blog
    </NavLink>
    <NavLink to="/contact" exact>
      Contact
    </NavLink>
  </>
);

export default function ThreeColumn({ children, aside, leftNav }) {
  return (
    <Layout
      header={<Header>Awesome Arcademy blog</Header>}
      topNav={<DefaultTopNav />}
      footer={
        <small>
          kleine lettertjes die iets over een copyright zeggen in 2020
        </small>
      }
      aside={aside}
      leftNav={leftNav}
    >
      {children}
    </Layout>
  );
}
