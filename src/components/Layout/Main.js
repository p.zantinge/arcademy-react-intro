import React from 'react';
import styled from 'styled-components';

const Container = styled.main`
  flex: 1 1 calc(100% - 32px);
`;

export default function Main({ children, ...rest }) {
  return <Container {...rest}>{children}</Container>;
}
