import React from 'react';
import styled from 'styled-components';

const width = 'calc(288px - 32px)';

const Container = styled.aside`
  flex: 0 0 ${width};
  min-width: ${width};
  max-width: ${width};
  background: lightgrey;
  padding: 16px;
`;

export default function Aside({ children, ...rest }) {
  return <Container {...rest}>{children}</Container>;
}
