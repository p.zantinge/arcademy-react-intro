import React from 'react';
import styled from 'styled-components';

const Container = styled.nav`
  background: silver;
  flex: 1 1 10px;
  padding-left: 32px;
`;

export default function TopNav({ children, ...rest }) {
  return <Container {...rest}>{children}</Container>;
}
