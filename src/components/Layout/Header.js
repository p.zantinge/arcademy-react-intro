import React from 'react';
import styled from 'styled-components';

const Container = styled.header`
  background: dimgrey;
  flex: 1 1 10px;
  padding: 8px;
`;

export default function Header({ children, ...rest }) {
  return <Container {...rest}>{children}</Container>;
}
