import React from 'react';
import styled from 'styled-components';

const width = 'calc(288px - 32px)';

const Container = styled.nav`
  flex: 0 0 ${width};
  min-width: ${width};
  max-width: ${width};
  background: darkgray;
  padding: 16px;
`;

export default function LeftNav({ children, ...rest }) {
  return <Container {...rest}>{children}</Container>;
}
