import React from 'react';
import Main from './Main';
import Header from './Header';
import TopNav from './TopNav';
import Aside from './Aside';
import Footer from './Footer';
import styled from 'styled-components';
import LeftNav from './LeftNav';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
`;

const Center = styled.div`
  display: flex;
  flex: 100000 1 100%;
`;

export default function Layout({
  children,
  header,
  aside,
  footer,
  topNav,
  leftNav,
  ...rest
}) {
  if (!children) {
    console.error('Children is a required prop');
    return null;
  }

  return (
    <Container {...rest}>
      {header && <Header>{header}</Header>}
      {topNav && <TopNav>{topNav}</TopNav>}
      <Center>
        {leftNav && <LeftNav>{leftNav}</LeftNav>}
        <Main aside={Boolean(aside)} leftNav={Boolean(leftNav)}>
          {children}
        </Main>
        {aside && <Aside>{aside}</Aside>}
      </Center>
      {footer && <Footer>{footer}</Footer>}
      {}
    </Container>
  );
}
