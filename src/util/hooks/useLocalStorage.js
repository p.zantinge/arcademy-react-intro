import { useEffect, useState } from 'react';

const types = {
  func: 'function',
  obj: 'object',
  arr: 'array',
  null: 'null',
  number: 'number',
  nan: 'NaN',
  bool: 'boolean',
  string: 'string',
  undefined: 'undefined',
};

// Expands the of types in JS a bit, because it is lacking
function realType(x) {
  const initialType = typeof x;
  if (initialType === 'object') {
    if (Array.isArray(x)) return 'array';
    if (x === null) return 'null';
  }

  if (initialType === 'number') if (isNaN(x)) return 'NaN';

  return initialType;
}

// Parses the value to string
function parseToString(val, type) {
  switch (type) {
    case types.func:
    case types.nan:
    case types.null:
    case types.undefined:
      return '';
    case types.obj:
    case types.arr:
    case types.number:
    case types.bool:
      return JSON.stringify(val);
    default:
      return val;
  }
}

function parseStringToType(val, type) {
  switch (type) {
    case types.func:
    case types.nan:
    case types.null:
      return '';
    case types.obj:
    case types.arr:
    case types.bool:
      return JSON.parse(val);
    case types.number:
      return parseFloat(val);
    default:
      return val;
  }
}

const invalidTypes = [types.nan, types.null, types.func, types.undefined];

export default function useLocalStorage(key, delay) {
  const [value, setValue] = useState();
  const typeOfValue = typeof value;

  // Saves it under the key in the localStorage if the value, key or type changes.
  useEffect(() => {
    if (!localStorage)
      console.error('localStorage is missing. What exotic browser is this?');

    if (invalidTypes.some(x => x === typeOfValue))
    {
      console.error(
        `This type (${typeOfValue}) of value will be stored as an empty string. So don't try to save it in localStorage.`
      );
    }
    else
    {
      const saveVal = parseToString(value, typeOfValue);
      localStorage.setItem(key, saveVal);
    }
  }, [value, key, typeOfValue]);

  // Gets the value from localStorage
  // Parses it to an implicit JS Object
  // Parses it to an explicit type passed in the arguments of load()
  // Returns the value
  function load() {
    if (!localStorage)
      console.error('localStorage is missing. What exotic browser is this?');
    const loadedVal = localStorage.getItem(key);
    console.log(typeOfValue);
    return JSON.parse(loadedVal);
  }

  function save(val) {
    setValue(val);
    return new Promise((resolve, reject) =>
      setTimeout(() => resolve(val), delay || 500)
    );
  }

  function clear() {
    localStorage.removeItem(key);
    return new Promise((resolve, reject) =>
      setTimeout(() => resolve(null), delay || 500)
    );
  }

  return { save, load, clear };
}
