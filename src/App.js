import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Examples from 'components/pages/examples';
import NotFound from 'components/pages/404';
import Home from 'components/pages/Home';
import Setup from 'components/pages/Setup';

function App() {
  return (
    <>
      <Switch>
        <Route path="/home" exact component={Home} />
        <Route path="/examples" component={Examples} />
        <Route path="/setup" component={Setup} />
        <Route path="/404" component={NotFound} />
        <Route path="/" exact render={() => <Redirect to="/home" />} />
        <Redirect to="/404" />
      </Switch>
    </>
  );
}

export default App;
